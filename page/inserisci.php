<?php
// inizializza sessione
session_start();

//  Verifica se l'utenet è gia loggato altrimenti lo rimanda al login
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ./login.php");
    exit;
}

?>

<?php include '../components/config.php' ?>

<?php $page = 'inserisci';
include '../components/header-dashboard.php' ?>


<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">INSERISCI</h4>
            </div>
            <div class="card-body">
                <form action="#" method="post">
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" name="nome" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label>Cognome</label>
                                <input type="text" name="cognome" class="form-control" value="">
                            </div>
                            <?php
                            
                            if (isset($_POST['invio'])) {

                                //definire variabili 
                                $nome = $_POST["nome"];
                                $cognome = $_POST["cognome"];

                                //sql per inserire nel DB
                                $sql = "INSERT INTO `anagrafia` 
                                (`id_name`, 
                                `nome`, 
                                `cognome`) 
                                VALUES 
                                (NULL, 
                                '$nome', 
                                '$cognome')";

                                if (mysqli_query($link, $sql)) {
                                    echo "Volto aggiunto correttamente";
                                } else {
                                    echo "Errore dell'inserimento: ";
                                }
                            }
                            ?>

                            <div class="spacer"></div>
                            <button class="btn-send" name="invio"> INVIA</button>
                        </div>
                        <div class="col-md-9 pr-1">
                            <div class="containerss">
                                <div class="drop-zone">
                                    <form action="#" method="POST">
                                        <span class="drop-zone__prompt">Trascina o clicca per caricare</span>
                                        <input type="file" name="myFile" class="drop-zone__input">

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div> 

</div>
</div>
</div>




<script src="/assets/js/preview_img_upload.js"></script>
<script src="/testing/script.js"></script>

<?php include '../components/footer-dashboard.php' ?>