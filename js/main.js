const video = document.getElementById("video");
let predictedAges = [];

Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri("./models"),
    faceapi.nets.faceLandmark68Net.loadFromUri("./models"),
    faceapi.nets.faceRecognitionNet.loadFromUri("./models"),
    faceapi.nets.faceExpressionNet.loadFromUri("./models"),
    faceapi.nets.ageGenderNet.loadFromUri("./models"),
    faceapi.nets.ssdMobilenetv1.loadFromUri('./models')
]).then(startVideo);

async function startVideo() {
    navigator.getUserMedia({ video: {} },
        stream => (video.srcObject = stream),
        err => console.error(err)
    );



    video.addEventListener("playing", async() => {
        const canvas = faceapi.createCanvasFromMedia(video);
        document.body.append(canvas);

        const labeledFaceDescriptors = await loadLabeledImages()
        const faceMatcher = await new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6);

        const displaySize = { width: video.width, height: video.height };
        faceapi.matchDimensions(canvas, displaySize);

        setInterval(async() => {
            const detections = await faceapi
                .detectAllFaces(video, new faceapi.TinyFaceDetectorOptions())
                .withFaceLandmarks()
                .withFaceExpressions()
                .withAgeAndGender()
                .withFaceDescriptors();

            if (!detections.length) {
                return
            }
            const resizedDetections = faceapi.resizeResults(detections, displaySize);
            canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);

            faceapi.draw.drawDetections(canvas, resizedDetections);
            faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
            faceapi.draw.drawFaceExpressions(canvas, resizedDetections);

            if (typeof resizedDetections[0] !== 'undefined') {

                const age = resizedDetections[0].age;
                const interpolatedAge = interpolateAgePredictions(age);
                const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))

                const bottomRight = {
                    x: resizedDetections[0].detection.box.bottomRight.x - 50,
                    y: resizedDetections[0].detection.box.bottomRight.y
                };
                const botBottomRight = {
                    x: resizedDetections[0].detection.box.bottomRight.x - 50,
                    y: resizedDetections[0].detection.box.bottomRight.y + 23
                };
                const bikiniBottom = {
                    x: resizedDetections[0].detection.box.bottomRight.x - 50,
                    y: resizedDetections[0].detection.box.bottomRight.y + 46
                };

                //Rilevatore età
                /*new faceapi.draw.DrawTextField(
                    [`${faceapi.utils.round(interpolatedAge, 0)} anni`],
                    bottomRight
                ).draw(canvas);*/

                new faceapi.draw.DrawTextField(
                    [`${detections[0].gender}`],
                    botBottomRight
                ).draw(canvas);

                new faceapi.draw.DrawTextField(
                    [`${results[0]._label}`],
                    bikiniBottom
                ).draw(canvas);
                results.forEach((result, i) => {
                    const box = resizedDetections[i].detection.box
                    const drawBox = new faceapi.draw.DrawBox(box, { boxColor: "#fff", lineWidth: 2 })
                    drawBox.draw(canvas)
                })

            }

        }, 100);
    });
}


function interpolateAgePredictions(age) {
    predictedAges = [age].concat(predictedAges).slice(0, 30);
    const avgPredictedAge =
        predictedAges.reduce((total, a) => total + a) / predictedAges.length;
    return avgPredictedAge;
}

function loadLabeledImages() {
    const labels = ['Davide Palladino', 'Antonio Domenico Schiavone', 'Lorena Laurenza', 'Pasquale Ferraresi']
    return Promise.all(
        labels.map(async label => {
            const descriptions = []
            for (let i = 1; i <= 2; i++) {
                const img = await faceapi.fetchImage(`./images/${label}/${i}.jpg`)
                const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
                descriptions.push(detections.descriptor)
            }
            return new faceapi.LabeledFaceDescriptors(label, descriptions)
        })
    )
}