<?php include "./components/config.php" ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Carlo</title>
    <link rel="stylesheet" href="css/styles.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>

<header>

    <nav class="menu">
        <a class="brand-menu" href="#">Logo</a>
        <!--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> -->
        <div class="item">
            <ul class="menu-list">
                <li class="menu-item ">
                    <a href="https://rilface.000webhostapp.com/riconoscimento.php" class="menu-link ">Riconoscimento</a>
                </li>
                <li class="menu-item right ">
                    <a href="https://rilface.000webhostapp.com/auth/login.php" class="menu-link "><i class="fa fa-user "></i></a>
                </li>
            </ul>
        </div>

    </nav>

</header>

<body>
    <video id="video" height="480" width="1000" autoplay muted></video>
    <script src="js/face-api.min.js"></script>
    <script src="js/main.js"></script>
    <!--Script riconosciemnto-->
</body>

</html>